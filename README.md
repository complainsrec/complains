# README #

### What is this repository for? ###

* Application to receive complains and provide information about them. 
* Version 0.0.1
* Found at: https://caioedai30@bitbucket.org/complainsrec/complains.git

### How do I get set up? ###

This is a springboot application. So every dependency and startup configuration is already there and no further
is required.

Just download the repository and perform a maven build. After it, just start the jar File inside "target" folder as:
java -jar complainsBackend-0.0.1-SNAPSHOT.jar

If you don't set which environment you are working in, spring will take it as Default. Otherwise, if you want to set the 
environment, just do as follows
java -jar -Dspring.profiles.active=dev complainsBackend-0.0.1-SNAPSHOT.jar
or 
java -jar -Dspring.profiles.active=hml complainsBackend-0.0.1-SNAPSHOT.jar
or
java -jar -Dspring.profiles.active=prod complainsBackend-0.0.1-SNAPSHOT.jar

### What about configuration? ###

Make sure port 8080 is available. If not, just go to application.properties file (inside src/main/resources) and set
the port property as follows (with the port of your wish)
server.port=${port:8080}

This application already contains a google key to access Google map services, however, if you are
having any trouble surrounding the key, you must generate one of your own by doing the following the steps.

*1) Visit developers.google.com/console and log in with a Google Account.
*2) Select one of your existing projects, or create a new project.
*3) Enable the API(s) you want to use. The Java Client for Google Maps Services accesses the following APIs: 
	Directions API
	Distance Matrix API
	Elevation API
	Geocoding API
	Places API
	Roads API
	Time Zone API
*4) Create a new Server key.
If you'd like to restrict requests to a specific IP address, do so now.

Get the key and change in application.properties file at the property:
google.api.key

Obs: Don't forget to set other application.properties environment files.

### How to config database? ###
This application uses H2. 
Look at application.properties to look for configuration and data access.
If you want to check-out the database while the application is running, so, just 
go at http://localhost:8080/h2/ and set file name to "Complains"

### How to run tests? ###
mvn clean install

### How to use the operations? ###
This application has four operations

*1) GetCompany
Just give a string to find its location. This API uses Google Map Services, so you can type anything you want to find a location.
The API returns all possible addresses containg full location description. After calling it and deciding which address matches your company (It was
made on that way to profit at maximum the whole funcionality of Geoloc system), just get the "placeId" to
perform the next operations. 

Important info: You MUST use the placeId to perform any other operation.

Ex:
Type: REST GET
URL: http://localhost:8080/auth/v1/company/v1/getPossibleAddresses
Param: location
Value: You decide (Ex: lanzhou)
Response on Body:
[
"{\n \"addressComponents\": [\n {\n \"longName\": \"Lanzhou\",\n \"shortName\": \"Lanzhou\",\n \"types\": [\n \"LOCALITY\",\n \"POLITICAL\"\n ]\n },\n {\n \"longName\": \"Gansu\",\n \"shortName\": \"Gansu\",\n \"types\": [\n \"ADMINISTRATIVE_AREA_LEVEL_1\",\n \"POLITICAL\"\n ]\n },\n {\n \"longName\": \"China\",\n \"shortName\": \"CN\",\n \"types\": [\n \"COUNTRY\",\n \"POLITICAL\"\n ]\n }\n ],\n \"formattedAddress\": \"Lanzhou, Gansu, China\",\n \"geometry\": {\n \"bounds\": {\n \"northeast\": {\n \"lat\": 36.9991549,\n \"lng\": 104.5772686\n },\n \"southwest\": {\n \"lat\": 35.5711355,\n \"lng\": 102.6012755\n }\n },\n \"location\": {\n \"lat\": 36.061089,\n \"lng\": 103.834303\n },\n \"locationType\": \"APPROXIMATE\",\n \"viewport\": {\n \"northeast\": {\n \"lat\": 36.2582568,\n \"lng\": 104.0432739\n },\n \"southwest\": {\n \"lat\": 35.8984926,\n \"lng\": 103.4788513\n }\n }\n },\n \"types\": [\n \"LOCALITY\",\n \"POLITICAL\"\n ],\n \"partialMatch\": false,\n \"placeId\": \"ChIJgzkMmbWQWjYRXCQNXS4ATJE\"\n}"
]

*2)AddComplain
To add a complain is quitte easy. Just call the following URL with the paramaters:
Type: REST POST
URL: http://localhost:8080/auth/v1/complain/v1/addComplain
Param: companyPlaceId (ex: ChIJgzkMmbWQWjYRXCQNXS4ATJE - Got in GetCompany)
Param: author (Any String)
Param: complainTitle (Any String)
Param: complain (Any String)
Response: Complain added successfully or an error Message

*3) GetComplains
To get all complains of a company just call the following URL with the placeId parameter obtained in GetCompany.
This operation is limited to return at maximum 10 complains to avoid  network flood and application's flood as well.
If you want something diferent like, return specific ranges or something by date, just pay me.
Type: REST GET
URL: http://localhost:8080/auth/v1/complain/v1/getComplains?
Param: companyPlaceId 
Value: Some Id got in GetCompany operation ex: ChIJgzkMmbWQWjYRXCQNXS4ATJE 
Response: (Always a JSON list)
[
{
"author": "eu",
"complainedDate": 1512347234733,
"complain": "Mais uma critica",
"complainTitle": "Mais uma"
}
]

*4) getAmountOfComplains
To get the total of complains in a place, just perform the following operation 
Type: REST GET
URL: http://localhost:8080/auth/v1/complain/v1/getAmountOfComplains
Param: companyPlaceId 
Value: Some Id got in GetCompany operation ex: ChIJgzkMmbWQWjYRXCQNXS4ATJE 
Response:
An integer in the body response informing the amount of complains
