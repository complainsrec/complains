/**
 * 
 */
package br.com.reclameaqui.complains.backend.complainsBackend.services.impl;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.reclameaqui.complains.backend.complainsBackend.exceptions.GeolocationException;

/**
 * @author caior
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AddressServiceImplTest {

	@InjectMocks
	private AddressServiceImpl addressService;
	/**
	 * Test method for {@link br.com.reclameaqui.complains.backend.complainsBackend.services.impl.AddressServiceImpl#getPossibleAddressesByLocation(java.lang.String)}.
	 */
	@Test
	public void testGetPossibleAddressesByLocation() {
		try {
			addressService.setGoogleMapsServiceKey("AIzaSyA_172ctquNHih2ZdWMJ4mX12iQbAKJnmg");
			List<String> possibleAddressesByLocation = addressService.getPossibleAddressesByLocation("lanzhou");
			System.out.println(possibleAddressesByLocation);
			
			assertTrue(possibleAddressesByLocation.get(0).contains("placeId"));
			
		} catch (GeolocationException e) {
			fail();
		}
	}
}
