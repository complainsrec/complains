package br.com.reclameaqui.complains.backend.complainsBackend.exposedinterfaces;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import br.com.reclameaqui.complains.backend.complainsBackend.db.model.Company;
import br.com.reclameaqui.complains.backend.complainsBackend.exposedinterfaces.model.ComplainDTO;
import br.com.reclameaqui.complains.backend.complainsBackend.services.impl.CompanyServiceImpl;
import br.com.reclameaqui.complains.backend.complainsBackend.services.impl.ComplaintServiceImpl;

@RunWith(SpringRunner.class)
@WebMvcTest(value = ComplainController.class, secure = false)
public class ComplainControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ComplaintServiceImpl complaintService;
	
	@MockBean
	private CompanyServiceImpl companyService;
	
	@Test
	public void testAddComplain() throws Exception {
		Mockito.when(
				companyService.getCompany(Mockito.anyString())
						).thenReturn(new Company());
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/auth/v1/complain/v1/addComplain")
				.accept(MediaType.APPLICATION_JSON).param("companyPlaceId", "ChIJgzkMmbWQWjYRXCQNXS4ATJE").param("author", "eu").param("complainTitle", "Nao gostei").param("complain", "funcionarios nao bons").
				contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		assertEquals(result.getResponse().getStatus(),200);
		assertTrue(result.getResponse().getContentAsString().contains("Complain added successfully"));
	}

	@Test
	public void testGetComplains() throws Exception {
		Mockito.when(
				companyService.getCompany(Mockito.anyString())
						).thenReturn(new Company());
		
		Mockito.when(
				complaintService.getComplainsById(Mockito.anyLong())
						).thenReturn(mockComplains());
		
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.get("/auth/v1/complain/v1/getComplains")
				.accept(MediaType.APPLICATION_JSON).param("companyPlaceId", "ChIJgzkMmbWQWjYRXCQNXS4ATJE").
				contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		assertEquals(result.getResponse().getStatus(),200);
		JSONAssert.assertEquals(getJSONComplainsMocked(), result.getResponse()
				.getContentAsString(), false);		
	}
	
	@Test
	public void testGetAmountsOfComplains() throws Exception {
		Mockito.when(
				companyService.getCompany(Mockito.anyString())
						).thenReturn(new Company());
		
		Mockito.when(
				complaintService.getComplainsById(Mockito.anyLong())
						).thenReturn(mockComplains());
		
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.get("/auth/v1/complain/v1/getAmountOfComplains")
				.accept(MediaType.APPLICATION_JSON).param("companyPlaceId", "ChIJgzkMmbWQWjYRXCQNXS4ATJE").
				contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		assertEquals(result.getResponse().getStatus(),200);
		
		//passou no casting, está OK
		int amountOfComplains = Integer.parseInt(result.getResponse().getContentAsString());
	}

	private List<ComplainDTO> mockComplains() {
		List<ComplainDTO> complains = new LinkedList<ComplainDTO>();
		ComplainDTO complainDTO = new ComplainDTO();
		complainDTO.setAuthor("eu");
		complainDTO.setComplain("Nao foi bom");
		complainDTO.setComplainTitle("Que chato");
		complainDTO.setComplainedDate(new Timestamp(1512325359728L));
		complains.add(complainDTO);
		
		return complains;
	}

	public String getJSONComplainsMocked() {
		return "[{\"author\":\"eu\",\"complainedDate\":1512325359728,\"complain\":\"Nao foi bom\",\"complainTitle\":\"Que chato\"}]";
	}
	
	

}
