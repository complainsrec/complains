package br.com.reclameaqui.complains.backend.complainsBackend.exposedinterfaces;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import br.com.reclameaqui.complains.backend.complainsBackend.services.impl.AddressServiceImpl;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CompanyController.class, secure = false)
public class CompanyControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private AddressServiceImpl addressService;

	@Test
	public void testGetPossibleAddressesByLocationNoParameter() throws Exception {
		addressService.setGoogleMapsServiceKey("AIzaSyA_172ctquNHih2ZdWMJ4mX12iQbAKJnmg");
		List<String> placeGeoLocMocked = placeGeoLocMocked();

		Mockito.when(addressService.getPossibleAddressesByLocation("lanzhou")).thenReturn(placeGeoLocMocked);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/auth/v1/company/v1/getPossibleAddresses")
				.accept(MediaType.APPLICATION_JSON);

		MvcResult result;

		result = mockMvc.perform(requestBuilder).andReturn();
		assertEquals(result.getResponse().getStatus(),400);
		assertEquals(result.getResponse().getErrorMessage(),"Required String parameter 'location' is not present");
	}

	@Test
	public void testGetPossibleAddressesByLocationSuccess() throws Exception {
		addressService.setGoogleMapsServiceKey("AIzaSyA_172ctquNHih2ZdWMJ4mX12iQbAKJnmg");
		List<String> placeGeoLocMocked = placeGeoLocMocked();

		Mockito.when(addressService.getPossibleAddressesByLocation("lanzhou")).thenReturn(placeGeoLocMocked);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/auth/v1/company/v1/getPossibleAddresses").param("location", "lanzhou").
				accept(MediaType.APPLICATION_JSON);

		MvcResult result;

		result = mockMvc.perform(requestBuilder).andReturn();
		assertEquals(result.getResponse().getStatus(),200);

		assertTrue(result.getResponse().getContentAsString().contains("ChIJgzkMmbWQWjYRXCQNXS4ATJE"));
	}
	
	private List<String> placeGeoLocMocked() {
		List<String> listReturnMocked = new LinkedList<String>();
		listReturnMocked.add(locationInfo());

		return listReturnMocked;
	}

	private String locationInfo() {
		return "{\r\n" + "  \"addressComponents\": [\r\n" + "    {\r\n"
				+ "      \"longName\": \"Lanzhou\",\r\n" + "      \"shortName\": \"Lanzhou\",\r\n"
				+ "      \"types\": [\r\n" + "        \"LOCALITY\",\r\n" + "        \"POLITICAL\"\r\n" + "      ]\r\n"
				+ "    },\r\n" + "    {\r\n" + "      \"longName\": \"Gansu\",\r\n"
				+ "      \"shortName\": \"Gansu\",\r\n" + "      \"types\": [\r\n"
				+ "        \"ADMINISTRATIVE_AREA_LEVEL_1\",\r\n" + "        \"POLITICAL\"\r\n" + "      ]\r\n"
				+ "    },\r\n" + "    {\r\n" + "      \"longName\": \"China\",\r\n" + "      \"shortName\": \"CN\",\r\n"
				+ "      \"types\": [\r\n" + "        \"COUNTRY\",\r\n" + "        \"POLITICAL\"\r\n" + "      ]\r\n"
				+ "    }\r\n" + "  ],\r\n" + "  \"formattedAddress\": \"Lanzhou, Gansu, China\",\r\n"
				+ "  \"geometry\": {\r\n" + "    \"bounds\": {\r\n" + "      \"northeast\": {\r\n"
				+ "        \"lat\": 36.9991549,\r\n" + "        \"lng\": 104.5772686\r\n" + "      },\r\n"
				+ "      \"southwest\": {\r\n" + "        \"lat\": 35.5711355,\r\n" + "        \"lng\": 102.6012755\r\n"
				+ "      }\r\n" + "    },\r\n" + "    \"location\": {\r\n" + "      \"lat\": 36.061089,\r\n"
				+ "      \"lng\": 103.834303\r\n" + "    },\r\n" + "    \"locationType\": \"APPROXIMATE\",\r\n"
				+ "    \"viewport\": {\r\n" + "      \"northeast\": {\r\n" + "        \"lat\": 36.2582568,\r\n"
				+ "        \"lng\": 104.0432739\r\n" + "      },\r\n" + "      \"southwest\": {\r\n"
				+ "        \"lat\": 35.8984926,\r\n" + "        \"lng\": 103.4788513\r\n" + "      }\r\n" + "    }\r\n"
				+ "  },\r\n" + "  \"types\": [\r\n" + "    \"LOCALITY\",\r\n" + "    \"POLITICAL\"\r\n" + "  ],\r\n"
				+ "  \"partialMatch\": false,\r\n" + "  \"placeId\": \"ChIJgzkMmbWQWjYRXCQNXS4ATJE\"\r\n" + "}";
	}

}
