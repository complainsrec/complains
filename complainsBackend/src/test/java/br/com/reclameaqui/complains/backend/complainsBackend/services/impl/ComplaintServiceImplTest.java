package br.com.reclameaqui.complains.backend.complainsBackend.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.reclameaqui.complains.backend.complainsBackend.db.model.Company;
import br.com.reclameaqui.complains.backend.complainsBackend.exposedinterfaces.model.ComplainDTO;
import br.com.reclameaqui.complains.backend.complainsBackend.services.CompanyService;
import br.com.reclameaqui.complains.backend.complainsBackend.services.ComplaintService;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ComplaintServiceImplTest {
	
	@Autowired
	private ComplaintService complaintService;
	
	@Autowired
	private CompanyServiceImpl companyServiceTest;
	

	@TestConfiguration
	static class ComplaintServiceTestConfiguration {
	
		@Bean
		public ComplaintService complainService() {
			return new ComplaintServiceImpl();
		}
		
		@Bean
		public CompanyService companyService() {
			return new CompanyServiceImpl();
		}
	}

	@Test
	public void testAddComplain() {
		Company company = prepareBase();
		
		int initialAmount = complaintService.getAmountOfComplains(company.getId());
		
		complaintService.addComplain(company, "eu", "Mal atendimento", "Me mal-tratara");
		
		int afterAmount = complaintService.getAmountOfComplains(company.getId());
		
		assertEquals(afterAmount-initialAmount, 1);
		
		
	}

	@Test
	public void testGetComplainsById() {
		Company company = prepareBase();
		
		complaintService.addComplain(company, "eu", "Mal atendimento", "Me mal-tratara");
		
		List<ComplainDTO> complains = complaintService.getComplainsById(company.getId());
		
		assertTrue(complains.size()>0);
	}

	@Test
	public void testGetAmountOfComplains() {
		Company company = prepareBase();
		
		int initialAmount = complaintService.getAmountOfComplains(company.getId());
		
		complaintService.addComplain(company, "eu", "Mal atendimento", "Me mal-tratara");
		
		int afterAmount = complaintService.getAmountOfComplains(company.getId());
		
		assertEquals(afterAmount-initialAmount, 1);
	}

	private Company prepareBase() {
		Company company = companyServiceTest.getCompany("ChIJgzkMmbWQWjYRXCQNXS4ATJE");
		if (company.getId() == 0) {
			company.setGeolocPlaceId("ChIJgzkMmbWQWjYRXCQNXS4ATJE");
			
			companyServiceTest.addCompany(company);		
		}
		
		company = companyServiceTest.getCompany("ChIJgzkMmbWQWjYRXCQNXS4ATJE");
		return company;
	}

}
