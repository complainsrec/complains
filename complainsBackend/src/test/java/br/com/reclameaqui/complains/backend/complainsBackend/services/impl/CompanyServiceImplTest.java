package br.com.reclameaqui.complains.backend.complainsBackend.services.impl;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.reclameaqui.complains.backend.complainsBackend.db.model.Company;
import br.com.reclameaqui.complains.backend.complainsBackend.services.CompanyService;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CompanyServiceImplTest {	
	
	@Autowired
	private CompanyServiceImpl companyServiceTest;
	

	@TestConfiguration
	static class CompanyServiceTestConfiguration {
	
		@Bean
		public CompanyService companyService() {
			return new CompanyServiceImpl();
		}
	}
	
	@Test
	public void testAddCompany() {
		Company company = new Company();
		company.setGeolocPlaceId("ChIJgzkMmbWQWjYRXCQNXS4ATJE");
			
		companyServiceTest.addCompany(company);		
		
		Company companyAfterAdded = companyServiceTest.getCompany("ChIJgzkMmbWQWjYRXCQNXS4ATJE");
		
		assertNotNull(companyAfterAdded);
	}
	
	@Test
	public void testGetCompany() {
		Company companyAfterAdded = companyServiceTest.getCompany("ChIJgzkMmbWQWjYRXCQNXS4ATJE");
		
		assertNotNull(companyAfterAdded);
	}

	

}
