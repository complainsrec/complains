package br.com.reclameaqui.complains.backend.complainsBackend.services.impl;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.reclameaqui.complains.backend.complainsBackend.common.Constants;
import br.com.reclameaqui.complains.backend.complainsBackend.db.model.Company;
import br.com.reclameaqui.complains.backend.complainsBackend.db.model.Complain;
import br.com.reclameaqui.complains.backend.complainsBackend.exposedinterfaces.model.ComplainDTO;
import br.com.reclameaqui.complains.backend.complainsBackend.services.ComplaintService;

/**
 * A service responsible tocapture complains and provide information about 
 * @author caior
 *
 */
@Service
public class ComplaintServiceImpl implements ComplaintService {

	@PersistenceContext
	private EntityManager em;
	
	private Logger logger = Logger.getLogger(ComplaintServiceImpl.class);
	
	/**
	 * Adds a complain to the databse
	 * @param company
	 */
	@Override
	@Transactional
	public void addComplain(Company company, String author, String complainTitle, String complain) {
		Complain complainObj = new Complain();
		
		complainObj.setAuthor(author);
		complainObj.setComplainTitle(complainTitle);
		complainObj.setComplain(complain);
		complainObj.setComplainedDate(new Timestamp(System.currentTimeMillis()));
		
		complainObj.setCompany(company);
		
		logger.info("Init persist of a complain of company with internal Id: "+ company.getId());
		em.persist(complainObj);		
		logger.info("End persist of a complain of company with internal Id: "+ company.getId());
	}
	
	/**
	 * Gives the last 10 complains of a company. This request is limited to the last 10
	 * complains to avoid network flood and this application's flood.
	 * @param companyPlaceId Use the id given in getPossibleAddresses in Company's controller to indentify the company
	 * @return A list containg all complains
	 */
	public List<ComplainDTO> getComplainsById(long companyId) {
		logger.info("Getting complains of a complain of company with internal Id: "+ companyId);
		
		List<Complain> complains = (List<Complain>)em.createQuery(
			    "SELECT complain FROM Complain complain WHERE complain.company.id = :companyId")
			    .setParameter("companyId", companyId).setMaxResults(Constants.MAX_GET_RESULT).getResultList();
		
		logger.info("Got complains of a complain of company with internal Id: "+ companyId);
		
		
		List<ComplainDTO> complainsTransformed = new LinkedList<ComplainDTO>();
		
		for (Complain complain: complains) {
			ComplainDTO complainDTO = new ComplainDTO();
			complainsTransformed.add(complainDTO);
			
			BeanUtils.copyProperties(complain, complainDTO);
		}
		
		return complainsTransformed;
	}

	/**
	 * Gets the amount of complains of a company
	 * @param companyPlaceId Use the id given in getPossibleAddresses in Company's controller to indentify the company
	 * @return The amount of complains
	 */
	@Override
	public int getAmountOfComplains(long companyId) {
		logger.info("Getting amount of a complain of company with internal Id: "+ companyId);
		long amount = (long) em.createQuery("SELECT COUNT(*) FROM Complain complain WHERE complain.company.id = :companyId").
				setParameter("companyId", companyId).getSingleResult();
		logger.info("Got amount of a complain of company with internal Id: "+ companyId);
		
		return (int)amount;		
	}
}
