package br.com.reclameaqui.complains.backend.complainsBackend.services;

import java.util.List;

import br.com.reclameaqui.complains.backend.complainsBackend.exceptions.GeolocationException;

/**
 * Gets geoloc information about a location
 * @author caior
 *
 */
public interface AddressService {

	/**
	 * Returns all full data about some location in JSON format 
	 * 
	 * 
	 * @param location Anything related to location. Street name, company's name, postal code, longitute, latidude, position. At end of story, anything
	 * @return A list contaning all possible addresses about some company/location. Each entry contains a thing called "placeId" that must be used for further operations
	 */
	public List<String> getPossibleAddressesByLocation(String location) throws GeolocationException;
}
