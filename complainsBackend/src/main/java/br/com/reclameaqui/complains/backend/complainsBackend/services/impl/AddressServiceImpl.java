package br.com.reclameaqui.complains.backend.complainsBackend.services.impl;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;

import br.com.reclameaqui.complains.backend.complainsBackend.exceptions.GeolocationException;
import br.com.reclameaqui.complains.backend.complainsBackend.services.AddressService;

/**
 * Gets geoloc information about a location
 * @author caior
 *
 */
@Service
public class AddressServiceImpl implements AddressService {
	@Value("${google.api.key}")
	private String googleMapsServiceKey;

	/**
	 * Returns all full data about some location in JSON forma 
	 * 
	 * 
	 * @param location Anything related to location. Street name, company's name, postal code, longitute, latidude, position. At end of story, anything
	 * @return A list containing all possible addresses about some company/location. Each entry contains a thing called "placeId" that must be used for further operations
	 */
	@Override
	public List<String> getPossibleAddressesByLocation(String location) throws GeolocationException {

		try {
			GeoApiContext context = new GeoApiContext.Builder().apiKey(googleMapsServiceKey).build();
			GeocodingResult[] results;
			results = GeocodingApi.geocode(context, location).await();

			Gson gson = new GsonBuilder().setPrettyPrinting().create();

			List<String> possibleAddresses = new LinkedList<String>();

			for (GeocodingResult result : results) {
				possibleAddresses.add(gson.toJson(result));
			}

			return possibleAddresses;

		} catch (ApiException | InterruptedException | IOException e) {
			throw new GeolocationException(e);
		}

	}

	public void setGoogleMapsServiceKey(String googleMapsServiceKey) {
		this.googleMapsServiceKey = googleMapsServiceKey;
	}

}
