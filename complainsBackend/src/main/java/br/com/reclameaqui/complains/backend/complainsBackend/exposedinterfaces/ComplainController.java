package br.com.reclameaqui.complains.backend.complainsBackend.exposedinterfaces;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.reclameaqui.complains.backend.complainsBackend.db.model.Company;
import br.com.reclameaqui.complains.backend.complainsBackend.exposedinterfaces.model.ComplainDTO;
import br.com.reclameaqui.complains.backend.complainsBackend.services.CompanyService;
import br.com.reclameaqui.complains.backend.complainsBackend.services.ComplaintService;

/**
 * A controller responsible to capture complains and provide information about 
 * @author caior
 *
 */
@RestController
@RequestMapping("/auth/v1/complain/v1")
public class ComplainController {

	@Autowired
	private ComplaintService complaintService;
	
	@Autowired 
	private CompanyService companyService;

	/**
	 * A post receiver responsible to capture a complain about a company.
	 * @param companyPlaceId Use the id given in getPossibleAddresses in Company's controller to indentify the company
	 * @param author The complaint author
	 * @param complainTitle Complain's title
	 * @param complain The complain description
	 * @return "Complain added successfully" in Body's responde. 
	 */
	@PostMapping(path="/addComplain", produces=MediaType.APPLICATION_JSON_VALUE)
	public String addComplain(@RequestParam(name="companyPlaceId") String companyPlaceId, @RequestParam(name="author") String author, @RequestParam(name="complainTitle") String complainTitle, @RequestParam(name="complain") String complain) {
		Company company = companyService.getCompany(companyPlaceId);
		
		if (company.getGeolocPlaceId() == null) {
			company.setGeolocPlaceId(companyPlaceId);
			companyService.addCompany(company);
		}
		
		complaintService.addComplain(company, author, complainTitle, complain);
		
		return "Complain added successfully";
	}
	
	/**
	 * A get request receiver that provides the last 10 complains of a company. This request is limited to the last 10
	 * complains to avoid  network flood and this application's flood.
	 * @param companyPlaceId Use the id given in getPossibleAddresses in Company's controller to indentify the company
	 * @return The complains in JSON format.
	 */
	@GetMapping(path="/getComplains")
	public List<ComplainDTO> getComplains(@RequestParam(name="companyPlaceId") String companyPlaceId) {
		Company company = companyService.getCompany(companyPlaceId);
		List<ComplainDTO> complains = complaintService.getComplainsById(company.getId());		
		
		return complains;	
	}
	
	/**
	 * A get request receiver responsible to get the amount of complains of a company
	 * @param companyPlaceId Use the id given in getPossibleAddresses in Company's controller to indentify the company
	 * @return The amount of complains
	 */
	@GetMapping(path="/getAmountOfComplains")
	public int getAmountsOfComplains(@RequestParam(name="companyPlaceId") String companyPlaceId) {
		Company company = companyService.getCompany(companyPlaceId);
		
		int amountOfComplains = complaintService.getAmountOfComplains(company.getId());
		
		return amountOfComplains;
	}	
}
