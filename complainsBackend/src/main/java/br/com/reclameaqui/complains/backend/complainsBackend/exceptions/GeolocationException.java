package br.com.reclameaqui.complains.backend.complainsBackend.exceptions;

/**
 * An exception that wrapps a geolocation error while tries to access the geo
 * location system.
 * 
 * @author caior
 *
 */
public class GeolocationException extends Exception {

	/**
	 * 
	 * @param e The former geolocation system exception
	 */
	public GeolocationException(Exception e) {
		super(e);
	}
}
