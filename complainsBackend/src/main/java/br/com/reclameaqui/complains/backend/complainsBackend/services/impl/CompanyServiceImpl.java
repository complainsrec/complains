package br.com.reclameaqui.complains.backend.complainsBackend.services.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.reclameaqui.complains.backend.complainsBackend.db.model.Company;
import br.com.reclameaqui.complains.backend.complainsBackend.services.CompanyService;

/**
 * Service to get and add information about a company
 * @author caior
 *
 */
@Service
public class CompanyServiceImpl implements CompanyService {

	@PersistenceContext
	private EntityManager em;

	private Logger logger = Logger.getLogger(CompanyServiceImpl.class);
	
	/**
	 * Returns the company object through the geolocational id
	 * @param companyPlaceId 
	 * @return The company database's object
	 */
	@Override
	public Company getCompany(String companyPlaceId) {
		logger.info("Getting companie's Id byPlaceId: "+ companyPlaceId);
		
		List<Company> companies = (List<Company>)em.createQuery(
			    "SELECT company FROM Company company WHERE company.companyPlaceId = :companyPlaceId")
			    .setParameter("companyPlaceId", companyPlaceId).getResultList();
		
		if (companies.size() == 0) {
			logger.info("Companie's Id not found by companyPlaceId: "+ companyPlaceId);
			return new Company();
		} else {
			logger.info("Got companie's Id byPlaceId: "+ companyPlaceId);
			return companies.get(0);
		}
	}

	@Override
	@Transactional
	public void addCompany(Company company) {
		em.persist(company);		
	}	
}
