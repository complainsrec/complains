package br.com.reclameaqui.complains.backend.complainsBackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main class which starts springboot
 * 
 * @author caior
 *
 */
@SpringBootApplication
public class ComplainsBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComplainsBackendApplication.class, args);
	}
}
