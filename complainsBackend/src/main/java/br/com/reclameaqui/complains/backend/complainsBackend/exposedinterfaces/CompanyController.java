package br.com.reclameaqui.complains.backend.complainsBackend.exposedinterfaces;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.reclameaqui.complains.backend.complainsBackend.exceptions.GeolocationException;
import br.com.reclameaqui.complains.backend.complainsBackend.services.AddressService;

/**
 * A controller responsible to provide services related to a company's information
 * @author caior
 *
 */
@RestController
@RequestMapping("/auth/v1/company/v1")
public class CompanyController {

	@Autowired
	private AddressService service;

	/**
	 * Returns all full data about some location in JSON format 
	 * 
	 * 
	 * @param location Anything related to location. Street name, company's name, postal code, longitute, latidude, position. At end of story, anything
	 * @return A list contaning all possible addresses about some company/location. Each entry contains a thing called "placeId" that must be used for further operations
	 */
	@GetMapping(path="/getPossibleAddresses", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<String> getPossibleAddressesByLocation(@RequestParam(name="location") String location) {
		try {

			return service.getPossibleAddressesByLocation(location);

		} catch (GeolocationException e) {
			e.printStackTrace();
		}

		return null;
	}	
}
