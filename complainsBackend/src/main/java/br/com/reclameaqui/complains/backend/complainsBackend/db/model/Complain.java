package br.com.reclameaqui.complains.backend.complainsBackend.db.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


/**
 * Complain's entity related to database
 * 
 * @author caior
 *
 */
@Entity
public class Complain {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column
	private String author;
	
	@Column
    private Timestamp complainedDate;
	
	@Column
	private String complain;
	
	@Column
	private String complainTitle;
	
	@ManyToOne
	private Company company;
	
	/**
	 * 
	 * @return Complain's title
	 */
	public String getComplainTitle() {
		return complainTitle;
	}

	/**
	 * Sets complain title
	 * 
	 * @param complainTitle Complain's title
	 */
	public void setComplainTitle(String complainTitle) {
		this.complainTitle = complainTitle;
	}

	
	/**
	 * Gets complain's description
	 * @return complain's description
	 */
	public String getComplain() {		
		return complain;
	}

	/**
	 * Gets which company this complains belongs to
	 * 
	 * @return Company related to that complain
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * Set's complain's company
	 * @param company
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * 
	 * @param complain Sets complain
	 */	
	public void setComplain(String complain) {
		this.complain = complain;
	}

	/**
	 * 
	 * @return Complain's id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets complain
	 * 
	 * @param id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Get's the author that made this complain
	 * @return Complain's author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * Set's the author that made this complain
	 * @param Complain's author
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * Get the date of the complain
	 * @return
	 */
	public Timestamp getComplainedDate() {
		return complainedDate;
	}

	/**
	 * Set the date of the complain
	 * @return
	 */
	public void setComplainedDate(Timestamp complainedDate) {
		this.complainedDate = complainedDate;
	}
}
