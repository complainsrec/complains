package br.com.reclameaqui.complains.backend.complainsBackend.db.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Company entity related to database persistance
 * 
 * @author caior
 *
 */
@Entity
public class Company {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column
	private String companyPlaceId;

	/**
	 * 
	 * @return Internal company Id
	 */
	public long getId() {
		return id;
	}
	
	
	/**
	 * Sets internal id 
	 *  
	 * @param id internal id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return geoloc id
	 */
	public String getGeolocPlaceId() {
		return companyPlaceId;
	}

	/**
	 * Sets geoloc id
	 * @param geolocPlaceId Geoloc d
	 */
	public void setGeolocPlaceId(String geolocPlaceId) {
		this.companyPlaceId = geolocPlaceId;
	}
}
