package br.com.reclameaqui.complains.backend.complainsBackend.common;

/**
 * 
 * Constants class
 * 
 * @author caior
 *
 */
public class Constants {
	/**Max results return by a query*/
	public static int MAX_GET_RESULT = 10;
}
