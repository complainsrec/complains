package br.com.reclameaqui.complains.backend.complainsBackend.services;

import java.util.List;

import br.com.reclameaqui.complains.backend.complainsBackend.db.model.Company;
import br.com.reclameaqui.complains.backend.complainsBackend.exposedinterfaces.model.ComplainDTO;

/**
 * A service responsible to capture complains and provide information about 
 * @author caior
 *
 */
public interface ComplaintService {

	/**
	 * Adds a complain about a company.
	 * @param companyPlaceId Use the id given in getPossibleAddresses in Company's controller to indentify the company
	 * @param author The complaint author
	 * @param complainTitle Complain's title
	 * @param complain The complain description
	 *  
	 */
	void addComplain(Company company, String author, String complainTitle, String complain);
	
	/**
	 * Gives the last 10 complains of a company. This request is limited to the last 10
	 * complains to avoid  network flood and this application's flood.
	 * @param companyPlaceId Use the id given in getPossibleAddresses in Company's controller to indentify the company
	 * @return A list containg all complains
	 */
	public List<ComplainDTO> getComplainsById(long companyId);
	
	/**
	 * Gets the amount of complains of a company
	 * @param companyPlaceId Use the id given in getPossibleAddresses in Company's controller to indentify the company
	 * @return The amount of complains
	 */
	public int getAmountOfComplains(long companyId);
}
