package br.com.reclameaqui.complains.backend.complainsBackend.services;

import br.com.reclameaqui.complains.backend.complainsBackend.db.model.Company;

/**
 * Service to get and add information about a company
 * @author caior
 *
 */
public interface CompanyService {

	/**
	 * Returns the company object through the geolocational id
	 * @param companyPlaceId 
	 * @return The company database's object
	 */
	Company getCompany(String companyPlaceId);
	
	/**
	 * Adds a company to the databse
	 * @param company
	 */
	void addCompany(Company company);
}
