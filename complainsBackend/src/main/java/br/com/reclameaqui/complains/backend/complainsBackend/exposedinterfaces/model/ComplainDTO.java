package br.com.reclameaqui.complains.backend.complainsBackend.exposedinterfaces.model;

import java.sql.Timestamp;

/**
 * Used to return data to client in controllers;
 * @author caior
 *
 */
public class ComplainDTO {

	private String author;	

    private Timestamp complainedDate;	

	private String complain;
	
	private String complainTitle;

	public String getAuthor() {
		return author;
	}

	public String getComplainTitle() {
		return complainTitle;
	}

	public void setComplainTitle(String complainTitle) {
		this.complainTitle = complainTitle;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Timestamp getComplainedDate() {
		return complainedDate;
	}

	public void setComplainedDate(Timestamp complainedDate) {
		this.complainedDate = complainedDate;
	}

	public String getComplain() {
		return complain;
	}

	public void setComplain(String complain) {
		this.complain = complain;
	}
}
